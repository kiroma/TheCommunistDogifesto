# The Communist Dogifesto
*In 1957, the Soviet Union launched Sputnik 2. On board the spacecraft was a
single dog, named Laika. When Laika returned to Earth, the Soviet scientists
discovered something profound: dogs in space experience extreme cognitive
enhancement. The following year the Soviet space program would be dedicated
solely to studying the effects of space on canine intelligence...*

***The Communist Dogifesto*** is a science fiction first-person shooter set on a
Soviet space station in Lunar orbit, which has been used as a platform for
experimentation on the Russian space dogs. Unfortunately, dogs this deep in
space have more tricks in their vac suit than the scientists accounted for. Now
Laika has taken over and you are the only one left who can take back the
station, or at least attempt to escape with your life!

In order to survive and defeat Laika and her robotic hench-dogs, you will have
to gain new abilities with tech upgrades, recycle scrap materials you find
lying around into usable items, read data logs written by the ousted humans to
find keycodes to explore new areas of the station, and do a lot of good old
fashioned shooting!

Features a hand-designed space station to explore, eight unique tech upgrades
to choose from, pages of in-universe data logs from the scientists and other
staff which reveal the nature of the work done on the station as well as vital
information to access new areas and items, and several different endings based
on in-game actions.

## Bugs

If you experience a bug or crash, you can report it on the GitLab repository
at https://gitlab.com/joshuagiles/TheCommunistDogifesto, or if you don't want
to make an account there, send an email to thecommunistdogifesto@gmail.com.

Please include as precise a description of what the problem is as you can
manage, otherwise it may be impossible to fix. If it is a crash, please include
the generated `crashlog.txt` which should be in the same directory as the game's
executable file.

## Compiling
Compiling the project should be a fairly straight-forward process, but you will
likely need to edit the makefile to prevent it trying to static link with libs
in the `src/libs/*` directory. Or for other reasons. I never tried compiling
the project on any computer besides my own so your mileage may vary. Either way
the only dependencies are SDL, OpenGL 3.3, and GLEW so it can't be too hard.

If you're a source diver and you find the cause of some bug or glitch, I'd
rather you just kindly let me know about it instead of submitting a pull request
with your own work. I'm still selling the game and I don't want to make money
from work you did for free. If I'm unresponsive or don't care or unreasonable,
nothing is stopping you from distributing the fixed game yourself (just not
commercially; the art assets are CC NC-BY-SA).

The code is pretty messy so I don't recommend anyone use it as a basis for
their own game. I release it under the GPL because I use open source software
almost exclusively to develop games, and I think people should have the ability
to look at what makes things work and have the freedom to change them... even
when what makes a thing work is a confusing, unoptimized mess, which is the
case here.

## Credits and Licensing
### Code
All the code here is distributed under the GPLv3 license, unless noted
otherwise. All non-GPL source files contain a copyright notice denoting the
particular license it is distributed under. For convenience I have also listed
each of them below. Thanks to:

*   **Lode Vandevenne** for *LodePNG* (zlib)
*   **David Reid** for *dr_wav* (public domain)
*   **Stefan Gustavson** for *noise1234* (public domain)
*   **Sean Barrett** for *stbvorbis* (public domain)
*   **Randy Gaul** for *tinyfiles.h* (zlib) which I have modified slightly
*   **datenwolf** for *linmath.h* (WTFPL) to which I have added several modifications

### Assets
All the audio and graphics (**except for the music**) are distributed under the
Creative Commons BY-NC-SA license, meaning you are free to redistribute them,
modified or otherwise, so long as you release the new product under the same
license, credit the original author (Joshua Giles), and don't distribute them
for commercial purposes.  https://creativecommons.org/licenses/by-nc-sa/4.0/

All the music was made by **Eric Matyas**, of www.soundimage.org, and is
licensed under the Soundimage International Public License which can be found
there.
