#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "procgl/procgl.h"
#include "bork.h"
#include "particle.h"
#include "entity.h"
#include "map_area.h"
#include "bullet.h"
#include "physics.h"
#include "upgrades.h"
#include "recycler.h"
#include "state_play.h"
#include "game_states.h"

static const struct bork_upgrade_detail BORK_UPGRADE_DETAIL[] = {
    [BORK_UPGRADE_JETPACK] = { .name = "JETPACK",
        .description = {
            "SPACE-AGE MANEUVERING SYSTEM",
            "ALLOWS LIMITED FLIGHT ABILITY.", } },
    [BORK_UPGRADE_DOORHACK] = { .name = "DOOR HACK",
        .description = {
            "RE-USABLE DOOR OVERRIDE PACKAGE",
            "ALLOWS UNLOCKING MOST DOORS IN",
            "THE STATION, WITHOUT A KEYCODE." },
        .active = { 1, 1 },
        .keep_first = 1 },
    [BORK_UPGRADE_BOTHACK] = { .name = "BOT HACK",
        .description = {
            "ROBOT OVERRIDE PACKAGE DAMAGES",
            "AND TEMPORARILY DISABLES ANY",
            "TARGETED ROBOT. UPDATED PACKAGE",
            "INCREASES RANGE AND DAMAGE." },
        .active = { 1, 1 } },
    [BORK_UPGRADE_DECOY] = { .name = "DECOY",
        .description = {
            "SECURITY BEACON DIRECTING ALL",
            "STATION ROBOTS TO TEMPORARILY",
            "FOCUS ON ONE LOCATION. UPDATE",
            "TO SELECT A LOCATION AT ANY",
            "DISTANCE." },
        .active = { 1, 1 } },
    [BORK_UPGRADE_HEALING] = { .name = "MEDICAL NANO-BOTS",
        .description = {
            "INTEGRATED MEDICAL NANO-BOTS",
            "PROVIDING PASSIVE TISSUE",
            "REGENERATION SERVICE WHEN",
            "THE USER IS BADLY WOUNDED.", },
        .active = { 0, 0 } },
    [BORK_UPGRADE_HEATSHIELD] = { .name = "HEAT SHIELD",
        .description = {
            "ALWAYS-ACTIVE FIRE RETARDANT",
            "EXO-SUIT PREVENTS USER FROM",
            "CATCHING FIRE AND MITIGATES",
            "HEAT AND RADIATION DAMAGE.",
            "UPDATE PROVIDES 100% IMMUNITY",
            "TO ALL HEAT DAMAGE." } },
    [BORK_UPGRADE_DEFENSE] = { .name = "DEFENSE FIELD",
        .description = {
            "ENERGETIC DEFENSE FIELD CAN",
            "DAMAGE ALL NEARBY ATTACKERS.",
            "UPDATE PROVIDES A CONSTANT",
            "PASSIVE PROTECTION FIELD",
            "WHICH REDUCES COMBAT DAMAGE." },
        .active = { 1, 1 },
        .keep_first = 0 },
    [BORK_UPGRADE_TELEPORTER] = { .name = "TRANSLOCATOR",
        .description = {
            "TRANSLOCATION BEACON ALLOWS",
            "USER TO TELEPORT INSTANTLY",
            "TO A PRE-SELECTED LOCATION.",
            "UPDATE TO ELIMINATE DISTANCE",
            "LIMITATION." },
        .active = { 1, 1 } },
    [BORK_UPGRADE_STRENGTH] = { .name = "STRENGTH",
        .description = {
            "MUSCLE ENHANCING NANO-BOTS",
            "GREATLY INCREASE MELEE DAMAGE",
            "AND MITIGATE FIREARM RECOIL.", } },
    [BORK_UPGRADE_SCANNING] = { .name = "SCANNING",
        .description = {
            "NEURAL IMPLANT PERFORMS PASSIVE",
            "SCANNING OF VISIBLE OBJECTS AND",
            "AUTOMATIC TRANSLATION OF ALL",
            "VISIBLE TEXT FROM RUSSIAN TO",
            "ENGLISH." },
        .active = { 1, 1 } },
};

const struct bork_upgrade_detail* bork_upgrade_detail(enum bork_upgrade u)
{
    return &BORK_UPGRADE_DETAIL[u];
}
